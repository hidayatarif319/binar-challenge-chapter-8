{
  "swagger": "2.0",
  "info": {
    "description": "Arif Hidayatullah - FSW 8",
    "version": "1.0.0",
    "title": "Arif API",
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "email": "hidayatarif319@gmail.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "arif.swagger.io",
  "basePath": "/api",
  "tags": [
    {
      "name": "players",
      "description": "Everything about Player"
    }
  ],
  "schemes": [
    "http",
    "https"
  ],
  "paths": {
    "/players": {
      "post": {
        "tags": [
          "players"
        ],
        "description": "Create a new player",
        "operationId": "playerCreate",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Player"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Player"
            }
          },
          "400": {
            "description": "Failed to create a new player",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "username or email or password field cannot be empty."
                }
              }
            }
          },
          "500": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Some error occurred while creating the Player."
                }
              }
            }
          }
        }
      },
      "get": {
        "tags": [
          "players"
        ],
        "description": "Find all players",
        "operationId": "playerFindAll",
        "parameters": [
          {
            "in": "query",
            "name": "username",
            "description": "Player username",
            "type": "string"
          },
          {
            "in": "query",
            "name": "email",
            "description": "Player email",
            "type": "string"
          },
          {
            "in": "query",
            "name": "experience",
            "description": "Player experience",
            "type": "integer"
          },
          {
            "in": "query",
            "name": "lvl",
            "description": "Player level",
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Player"
            }
          },
          "500": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Some error occurred while retrieving players."
                }
              }
            }
          }
        }
      }
    },
    "/players/:id": {
      "put": {
        "tags": [
          "players"
        ],
        "description": "Update player by an id",
        "operationId": "playerUpdate",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Player"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Player was updated successfully."
                }
              }
            }
          },
          "400": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Cannot update Player with id=${id}. Maybe Player was not found or req.body is empty!"
                }
              }
            }
          },
          "500": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Error updating Player with id="
                }
              }
            }
          }
        }
      },
      "get": {
        "tags": [
          "players"
        ],
        "description": "Find player by id",
        "operationId": "playerFindById",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Player"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/Player"
            }
          },
          "500": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Error retrieving Player with id="
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "players"
        ],
        "description": "Delete player by id",
        "operationId": "playerDelete",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Player"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Player was deleted successfully!"
                }
              }
            }
          },
          "400": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Cannot delete Player with id=${id}. Maybe Player was not found!"
                }
              }
            }
          },
          "500": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Could not delete Player with id="
                }
              }
            }
          }
        }
      }
    },
    "/players/exp/:id": {
      "post": {
        "tags": [
          "players"
        ],
        "description": "Add player experience by certain amount",
        "operationId": "playerGetExperience",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Add player experience by certain amount",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Player"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Player with id=${id} has more experience."
                }
              }
            }
          },
          "400": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Cannot update Player with id=${id}!"
                }
              }
            }
          },
          "500": {
            "description": "Failed",
            "schema": {
              "type": "object",
              "properties": {
                "message": {
                  "type": "string",
                  "example": "Error updating Player exp with id="
                }
              }
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Player": {
      "type": "object",
      "required": [
        "name"
      ],
      "properties": {
        "username": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "password": {
          "type": "string"
        },
        "experience": {
          "type": "integer"
        },
        "lvl": {
          "type": "integer"
        }
      }
    }
  }
}