import React, { Component } from "react";

class Text extends Component {
  render() {
    const { value, placeholder, name, onChange } = this.props;

    return (
      <input
        type="text"
        onChange={onChange}
        name={name}
        value={value}
        placeholder={placeholder}
      />
    );
  }
}

export default Text;
