import React, { Component } from "react";

class Password extends Component {
  render() {
    const { value, placeholder, name, onChange } = this.props;

    return (
      <input
        type="password"
        onChange={onChange}
        name={name}
        value={value}
        placeholder={placeholder}
      />
    );
  }
}

export default Password;
