import Text from "../components/Text/index";
import Password from "../components/Password/index";
import { Component } from "react";
import Email from "../components/Email";
import Level from "../components/Level";
import Exp from "../components/Exp";

class CreatePlayer extends Component {
  state = {
    username: "",
    email: "",
    password: "",
    exp: "",
    level: "",
    showData: false,
  };

  handleFieldChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      showData: false,
    });
  };

  handleAddData = () => {
    this.setState({
      showData: true,
    });
  };

  render() {
    return (
      <div>
        <h1>Form Create Player</h1>
        <Text
          name="username"
          placeholder="Input your username"
          onChange={this.handleFieldChange}
          value={this.state.username}
        />
        <br />
        <Email
          name="email"
          placeholder="Input your email"
          onChange={this.handleFieldChange}
          value={this.state.email}
        />
        <br />
        <Password
          name="password"
          placeholder="Input your password"
          onChange={this.handleFieldChange}
          value={this.state.password}
        />
        <br />
        <Exp
          name="exp"
          placeholder="Input your exp"
          onChange={this.handleFieldChange}
          value={this.state.exp}
        />
        <br />
        <Level
          name="level"
          placeholder="Input your level"
          onChange={this.handleFieldChange}
          value={this.state.level}
        />
        <br />
        <button onClick={this.handleAddData}> Submit </button>
        <br />
        {this.state.showData && (
          <ul>
            <li>Username : {this.state.username} </li>
            <li>Email : {this.state.email} </li>
            <li>Player Created!</li>
          </ul>
        )}
      </div>
    );
  }
}

export default CreatePlayer;
