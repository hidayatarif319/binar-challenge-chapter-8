import { Component } from "react";
import "./App.css";
import CreatePlayer from "./pages/CreatePlayer";
import EditPlayer from "./pages/EditPlayer";
import FindPlayer from "./pages/FindPlayer";

class App extends Component {
  state = {
    activeMenu: 'Create Player'
  }

  handleChangeMenu = (event) => {
    this.setState({activeMenu: event.target.id});
  }

  render() {
    return (
      <div className="App">
        <button id="Create Player" onClick={this.handleChangeMenu}>Create Player</button>
        <button id="Edit Player" onClick={this.handleChangeMenu}>Edit Player</button>
        <button id="Find Player" onClick={this.handleChangeMenu}>Find Player</button>
        <br/>
        {this.state.activeMenu === "Create Player" && <CreatePlayer/>}
        {this.state.activeMenu === "Edit Player" && <EditPlayer/>}
        {this.state.activeMenu === "Find Player" && <FindPlayer/>}
      </div>
    );
  }
}

export default App;
